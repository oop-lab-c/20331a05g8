#include<iostream>
using namespace std;
class A{
    public:
    virtual void m1(){
        cout<<"m1 for A"<<endl;
    }
    void m2(){
        cout<<"m2 for A"<<endl;
    }
};
class B : public A{
    public:
    void m1(){
        cout<<"m1 in B"<<endl;
    }
    void m2(){
        cout<<"m2 in B"<<endl;
    }
};
int main(int argc, char const *argv[])
{
    A obj;
    B obj1;
    A *a;
    cout<<"Base class pointer to base class object"<<endl;
    a = &obj;
    a->m1();
    a->m2();
    cout<<"\nBase class pointer to derived class object"<<endl;
    a = &obj1;
    a->m1();
    a->m2();
    return 0;
}