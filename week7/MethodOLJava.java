class MeOverload{
    int pro(int a, int b){
        return a*b;
    }
    double pro(double a, double b){
        return a*b;
    }
    int pro(int a,int b, int c){
        return a*(b*c);
    }
}
class Main {
    public static void main(String[] args) {
        MeOverload me = new MeOverload();
        System.out.println("The product = "+me.pro(1,12));
        System.out.println("The sum = "+me.pro(2.6,3.6));
        System.out.println("The product = "+me.pro(1,9,30));
    }    
}