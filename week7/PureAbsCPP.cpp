#include<iostream>
using namespace std;
class shape{
    public:
    virtual void name()=0;
};
class Polygon : public shape{
    public:
    void name(){
        cout<<"Polygon"<<endl;
    }
};
class Rhombus : public shape{
    public:
    void name(){
        cout<<"Rhombus"<<endl;
    }
};
int main(){
    shape *b;
    Polygon obj;
    b = &obj;
    b->name();
    Rhombus ob;
    b = &ob;
    b->name();
    return 0;
}